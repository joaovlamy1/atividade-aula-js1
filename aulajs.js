function converter(texto){
    
    tex = texto.toLowerCase();

    var convertido = '';
    
    for (char of tex) {
        switch(char) {
            case 'a': convertido += '1'; break;
            case 'b': convertido += '2'; break;
            case 'c': convertido += '3'; break;
            case 'd': convertido += '4'; break;
            case 'e': convertido += '5'; break;
            case 'f': convertido += '6'; break;
            case 'g': convertido += '7'; break;
            case 'h': convertido += '8'; break;
            case 'i': convertido += '9'; break;
            case 'j': convertido += '10'; break;
            case 'k': convertido += '11'; break;
            case 'l': convertido += '12'; break;
            case 'm': convertido += '13'; break;
            case 'n': convertido += '14'; break;
            case 'o': convertido += '15'; break;
            case 'p': convertido += '16'; break;
            case 'q': convertido += '17'; break;
            case 'r': convertido += '18'; break;
            case 's': convertido += '19'; break;
            case 't': convertido += '20'; break;
            case 'u': convertido += '21'; break;
            case 'v': convertido += '22'; break;
            case 'w': convertido += '23'; break;
            case 'x': convertido += '24'; break;
            case 'y': convertido += '25'; break;
            case 'z': convertido += '26'; break;
            case '.': convertido += '0'; break;
            case ',': convertido += '-1'; break;
        }
    }
    return convertido
}  

var teste = `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut suscipit condimentum justo eget volutpat. Morbi felis diam, pellentesque sed aliquam eu, aliquam vitae elit. Integer ut sapien est. Nulla at elit nec nisl dignissim viverra sit amet in libero. Nunc sed bibendum enim. Praesent pharetra semper nulla vitae interdum. Nunc interdum enim imperdiet porta bibendum. Aenean ex ipsum, placerat sed sollicitudin et, tempus non sapien. Suspendisse congue vulputate molestie. Cras auctor vehicula justo tincidunt accumsan. Pellentesque vel iaculis nibh. Aliquam scelerisque eleifend vestibulum. Phasellus non dictum eros.Praesent cursus laoreet ipsum, in porta nisi hendrerit eu.Pellentesque scelerisque felis ut nunc sagittis, quis ultricies nunc euismod.Curabitur quis neque in magna efficitur luctus mollis vel odio.In eu condimentum orci.Curabitur ut ex imperdiet, consectetur diam at, vestibulum risus.Nunc pharetra, est eu placerat dapibus, risus odio blandit ex, at aliquam enim augue sit amet lacus.Cras bibendum, quam non ultrices porttitor, leo urna egestas eros, a sagittis ligula erat vitae purus.In sit amet porta turpis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae Cras mauris mi, aliquet ac dui non, pellentesque venenatis metus.Integer hendrerit tortor id pharetra ultrices.Suspendisse cursus suscipit congue.Vestibulum ornare faucibus interdum.Aliquam dapibus elit sed lorem laoreet tincidunt.Duis et sem fermentum urna tincidunt rutrum sit amet volutpat elit.`;

console.log(converter(teste))
